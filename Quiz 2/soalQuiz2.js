/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 * 
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 * 
 * Selamat mengerjakan
*/

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
  constructor(points) {
      this.subject;
      this.points = points;
      this.email;
  }

  average() {
      let point = this.points;
      if(Array.isArray(point)) {
          let size = point.length;
          let sum = 0;
          for (let index = 0; index < size; index++) {
            sum += point[index];
          }
        return Number(sum / size).toFixed(1);
      }
      return point;
  }
}
// var input = [5,2,5];
// var test = new Score(input);
// console.log(test.average())

/*=========================================== 
2. SOAL Create Score (10 Poin + 5 Poin ES6)
===========================================
Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
Function viewScores mengolah data email dan nilai skor pada parameter array 
lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
Contoh: 

Input

data : 
[
  ["email", "quiz-1", "quiz-2", "quiz-3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88]
]
subject: "quiz-1"

Output 
[
  {email: "abduh@mail.com", subject: "quiz-1", points: 78},
  {email: "khairun@mail.com", subject: "quiz-1", points: 95},
]
*/

const data = [
["email", "quiz - 1", "quiz - 2", "quiz - 3"],
["abduh@mail.com", 78, 89, 90],
["khairun@mail.com", 95, 85, 88],
["bondra@mail.com", 70, 75, 78],
["regi@mail.com", 91, 89, 93]
]

let viewScores = (data, subject) => {

  let paramEmail = 0;
  let paramSubject = 0;
  let outputArr = [];
  switch (subject) {
      case "quiz - 1":
          paramSubject = 1;
          break;
      case "quiz - 2":
          paramSubject = 2;
          break;
      case "quiz - 3":
          paramSubject = 3;
          break;
  }

  for (let x = 1; x < data.length; x++) {
      let fromScores = new Score(data[x][paramSubject]);
      let outputObj = {};
      outputObj.email = data[x][paramEmail];
      outputObj.subject = subject;
      outputObj.points = fromScores.average();

      outputArr.push(outputObj);
  }
  console.log(outputArr);
}

// TEST CASE
viewScores(data, "quiz - 1")
viewScores(data, "quiz - 2")
viewScores(data, "quiz - 3")

/*==========================================
3. SOAL Recap Score (15 Poin + 5 Poin ES6)
==========================================
  Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
  Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
  predikat kelulusan ditentukan dari aturan berikut:
  nilai > 70 "participant"
  nilai > 80 "graduate"
  nilai > 90 "honour"

  output:
  1. Email: abduh@mail.com
  Rata-rata: 85.7
  Predikat: graduate

  2. Email: khairun@mail.com
  Rata-rata: 89.3
  Predikat: graduate

  3. Email: bondra@mail.com
  Rata-rata: 74.3
  Predikat: participant

  4. Email: regi@mail.com
  Rata-rata: 91
  Predikat: honour

*/

let recapScores = data => {

  let outputArr = [];
  for (let x = 1; x < data.length; x++) {
    let tempArray = [];
    let tempObject = {};
    let avg = 0;
    for (let y = 1; y < data[x].length; y++) {
        tempArray[y - 1] = data[x][y];
    }
    let fromScore = new Score(tempArray);
    avg = fromScore.average();
    if(avg > 0 && avg < 80) {
        tempObject.predicate = "participant";
    } else if(avg > 80 && avg < 90) {
        tempObject.predicate = "graduate";
    } else {
        tempObject.predicate = "honour";
    }
    tempObject.index = x;
    tempObject.email = data[x][0];
    tempObject.average = avg;
    outputArr.push(tempObject)
  }

  console.log(`Output: `);
  for (let x = 0; x < outputArr.length; x++) {
      console.log(`${outputArr[x].index}. Email: ${outputArr[x].email}
Rata-rata: ${outputArr[x].average} 
Predikat: ${outputArr[x].predicate}`);
console.log();
  }
}

recapScores(data);