//Tugas 4 – Functions

console.log('--------------- SOAL 1 ---------------');
function teriak() {
    console.log("Halo Sanbers!");   
}

teriak()

console.log('\n--------------- SOAL 2 ---------------');
function kalikan(num1, num2) {
    return num1 * num2
}

    var num1 = 12
    var num2 = 4
    var hasilKali = kalikan(num1, num2)
    
console.log(hasilKali) //48

//Soal 3
console.log('\n--------------- SOAL 3 ---------------'); //Judul
function introduce(name,age,address,hobby) {
    console.log("Nama saya " + name + ", umur saya " + age + ", alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!");
  }
  
  var name = "Agus";
  var age = 30;
  var address = "Jln. Malioboro, Yogjakarta";
  var hobby = "gaming";
  
  var perkenalan = introduce(name,age,address,hobby);
  console.log(perkenalan); // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogjakarta, dan saya punya hobby yaitu gaming!"
  



