//SOAL 1

console.log('--------------- SOAL 1 ---------------');
console.log('\nLOOPING PERTAMA');
var love = 2;
var jumlah = 0;
while(jumlah < 20) { 
  jumlah += love; 
  console.log(jumlah + ' - I love coding')
}
console.log('\nLOOPING KEDUA');
var developer = 2;
var jumlahdata = 20;
while(jumlahdata > 0) { 
   console.log(jumlahdata + ' - I will become a mobile developer')
  jumlahdata -= developer; 
}

//SOAL 2

console.log('\n--------------- SOAL 2 ---------------');
for(var angka = 1; angka <= 20; angka++) {
    if((angka%3 == 0 && angka%2==1)){
        console.log(angka + ' - I love coding')
    }else if (angka%2 ==0){
        console.log(angka + ' - Berkualitas')
    }else if (angka%2 !=0){
        console.log(angka + ' - Santai')
    }
    
  } 

//SOAL 3

console.log('\n--------------- SOAL 3 ---------------');
var pager = '';
for(var i = 0; i < 4; i++) {
    for(var a = 0; a < 8; a++) {
        pager += '#';
    }
    pager += '\n';
  } 
console.log(pager);

console.log('\n--------------- SOAL 4 ---------------');
var pager = '';
for(var i = 0; i < 8; i++) {
    for(var a = 0; a < i; a++) {
        pager += '#';
    }
    pager += '\n';
  } 
console.log(pager);

console.log('\n--------------- SOAL 5 ---------------');
var pager = '';
for(var i = 0; i < 8; i++) {
    for(var a = 0; a < 8; a++) {
        if(i%2==1){
            if(a%2==0){
                pager += '#';
            }else{
                pager += ' ';
            }  
        }else{
            if(a%2==0){
                pager += ' ';
            }else{
                pager += '#';
            }  
        }

    }
    pager += '\n';
  } 
console.log(pager); 