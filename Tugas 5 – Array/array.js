function rangeWithStep(startNum, stopNum, step) {

       if (typeof stopNum == 'undefined') {
    
           // one param defined
    
           stopNum = startNum;
    
           startNum = 0;
    
       }
    
       if (typeof step == 'undefined') {
    
           step = 1;
    
       }
    
       if ((step > 0 && startNum >= stopNum) || (step < 0 && startNum <= stopNum)) {
    
           return [];
    
       }
    
       var result = [];
    
       for (var i = startNum; step > 0 ? i < stopNum : i > stopNum; i += step) {
    
           result.push(i);
    
       }
    
       return result;
    
    };
    
